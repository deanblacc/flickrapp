//
//  ApiManager.swift
//  FlickrApp
//
//  Created by Dean Blacc on 19/01/2020.
//  Copyright © 2020 Dean Blacc. All rights reserved.
//

import Foundation
import UIKit

protocol ApiDelegate: class {
    func search(query: String, page: Int, completion: @escaping(([PhotoData]?, Int?, Error?) -> Void))
}

class ApiManager: ApiDelegate {

    static let shared = ApiManager()
    private let apikey = "3e7cc266ae2b0e0d78e279ce8e361736"
    private let pageSize = 100

    func search(
        query: String,
        page: Int,
        completion: @escaping(([PhotoData]?, Int?, Error?) -> Void)) {

        let url = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apikey)&format=json&nojsoncallback=1&safe_search=1&text=\(query)&per_page=\(pageSize)&page=\(page)")!

        URLSession.shared.dataTask(with: url) { (data, respsonse, error) in
            guard let data = data else { return }

            do {
                let decoder = JSONDecoder()
                let decodedData = try decoder.decode(FlickrResp.self, from: data)
                let photos = decodedData.info.photos
                let pages = decodedData.info.pages

                completion(photos, pages, nil)
            } catch {
                completion(nil, nil, error)
            }
        }.resume()
    }
}
