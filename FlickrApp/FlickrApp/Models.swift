//
//  Models.swift
//  FlickrApp
//
//  Created by Dean Blacc on 19/01/2020.
//  Copyright © 2020 Dean Blacc. All rights reserved.
//

import Foundation

struct PhotoData: Codable {
    let farm: Int
    let id: String
    let server: String
    let secret: String

    func urlForPhoto() -> URL {
        return URL(string: "http://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg")!
    }
}

struct FlickrResp: Codable {
    let info: PhotoResp

    private enum CodingKeys: String, CodingKey {
        case info = "photos"
    }
}

struct PhotoResp: Codable {
    let photos: [PhotoData]
    let pages: Int
    let page: Int

    private enum CodingKeys: String, CodingKey {
        case photos = "photo"
        case pages
        case page
    }
}
