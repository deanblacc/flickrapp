//
//  ViewController.swift
//  FlickrApp
//
//  Created by Dean Blacc on 18/01/2020.
//  Copyright © 2020 Dean Blacc. All rights reserved.
//

import UIKit

protocol MainViewControllerDelegate: class {
    func didRequestNextPage(query: String, page: Int, completion: @escaping (([PhotoData]) -> Void))
    func didRequestSearch(query: String, completion: @escaping(([PhotoData], Int) -> Void))
}

class MainViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activityLoader: UIActivityIndicatorView!
    @IBOutlet var searchBar: UISearchBar!

    let cellsPerRow = 3
    var query = "Mountains"
    var page = 1
    var totalPages = 1

    weak var delegate: MainViewControllerDelegate?

    var picsData = [PhotoData]() {
        didSet {
            DispatchQueue.main.async {
                if self.picsData.count > 0 {
                    self.collectionView.reloadData()
                    self.collectionView.isHidden = false
                } else {
                    self.collectionView.isHidden = true
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Picture This!"
        navigationController?.navigationBar.prefersLargeTitles = true

        collectionView.delegate = self
        collectionView.dataSource = self

        collectionView.contentInsetAdjustmentBehavior = .always

        activityLoader.style = .large
        activityLoader.startAnimating()

        searchBar.placeholder = "Search Me!"
        searchBar.delegate = self
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let w = collectionView.bounds.width / CGFloat(cellsPerRow)

        return CGSize(width: w, height: w)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
}

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        picsData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "picCellId", for: indexPath) as! PhotoCell

        // set a gradient like background colour on the collectionview as a placeholder
        let norm = CGFloat(indexPath.item) / 100.0
        cell.backgroundColor = UIColor(red: norm, green: 0.1 + norm, blue: 0.2 + norm, alpha: 1)

        if indexPath.item == picsData.count - 25 && page + 1 <= totalPages {
            page += 1
            
            delegate?.didRequestNextPage(query: query, page: page) { (photoData) in
                self.picsData += photoData
            }
        }

        let photo = picsData[indexPath.item]

        cell.configure(with: photo)

        return cell
    }
}

extension MainViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)

        guard let q = searchBar.text, !q.isEmpty else { return }
        query = q.replacingOccurrences(of: " ", with: "%20")

        delegate?.didRequestSearch(query: query) { (photos, pages) in
            DispatchQueue.main.async {
                self.collectionView.setContentOffset(.zero, animated: false)

                self.picsData = photos
                self.totalPages = pages
            }
        }
    }
}

class PhotoCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!

    func configure(with photoData: PhotoData) {
        imageView.image = nil
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image(at: photoData.urlForPhoto())
    }
}

extension UIImageView {
    func image(at url:URL) {
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: url) else { return }

            DispatchQueue.main.async {
                self.alpha = 0
                self.image = UIImage(data: imageData)
                UIView.animate(
                    withDuration: 0.3,
                    animations: {
                        self.alpha = 1.0
                    },
                    completion: nil
                )
            }
        }
    }
}
