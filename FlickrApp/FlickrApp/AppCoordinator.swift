//
//  AppCoordinator.swift
//  FlickrApp
//
//  Created by Dean Blacc on 18/01/2020.
//  Copyright © 2020 Dean Blacc. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator {
    func start() -> Void
}

class AppCoordinator: Coordinator {

    let nav: UINavigationController
    let apiDelegate: ApiDelegate

    init(nav: UINavigationController, apiDelegate: ApiDelegate) {
        self.nav = nav
        self.apiDelegate = apiDelegate
    }

    func start() {
        // load our initial view
        let mainView = UIStoryboard.mainView()
        mainView.delegate = self

        nav.pushViewController(mainView, animated: true)

        apiDelegate.search(query: mainView.query, page: mainView.page, completion: { (photos, pages, err) in
            guard let photos = photos, let pages = pages else { return }

            DispatchQueue.main.async {
                mainView.picsData = photos
                mainView.totalPages = pages
                mainView.activityLoader.stopAnimating()
                mainView.activityLoader.isHidden = true
            }
        })
    }
}

extension AppCoordinator: MainViewControllerDelegate {

    func didRequestSearch(query: String, completion: @escaping (([PhotoData], Int) -> Void)) {
        apiDelegate.search(query: query, page: 1) { (photoData, pages, error) in

            guard let photoData = photoData, let pages = pages else { return }

            completion(photoData, pages)
        }
    }

    func didRequestNextPage(query: String, page: Int, completion: @escaping (([PhotoData]) -> Void)) {
        apiDelegate.search(query: query, page: page, completion: { (photoData, pages, error) in

            guard let photoData = photoData else { return }

            completion(photoData)
        })
    }
}

extension UIStoryboard {
    private static let main = UIStoryboard(name: "Main", bundle: nil)

    class func mainView() -> MainViewController {
        return UIStoryboard.main.instantiateViewController(withIdentifier: "mainViewController") as! MainViewController
    }
}
