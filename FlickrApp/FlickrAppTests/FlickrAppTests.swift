//
//  FlickrAppTests.swift
//  FlickrAppTests
//
//  Created by Dean Blacc on 20/01/2020.
//  Copyright © 2020 Dean Blacc. All rights reserved.
//

import XCTest
@testable import FlickrApp

class ApiMock: ApiDelegate {
    static let shared = ApiMock()

    func search(query: String, page: Int, completion: @escaping (([PhotoData]?, Int?, Error?) -> Void)) {
        var res = [PhotoData]()
        for i in 0...9 {
            res.append(PhotoData(farm: i, id: "t", server: "es", secret: "t"))
        }

        completion(res, 1, nil)
    }
}

class ApiMockNoResults: ApiDelegate {
    static let shared = ApiMockNoResults()

    func search(query: String, page: Int, completion: @escaping (([PhotoData]?, Int?, Error?) -> Void)) {
        completion([], 0, nil)
    }
}


class TestModels: XCTestCase {
    func testPhotoDataURL() {
        // given
        let p = PhotoData(farm: 0, id: "t", server: "est", secret: "ing")

        // when
        let url = p.urlForPhoto()

        // then
        XCTAssertEqual(url.absoluteString, "http://farm0.static.flickr.com/est/t_ing.jpg")
    }
}

class TestMainView: XCTestCase {
    func testShowsResults() {
        // given
        let mock = ApiMock.shared
        let vc = UIStoryboard.mainView()
        let _ = vc.view

        // when
        mock.search(query: "Rabbits", page: 1) { (photos, pages, err) in
            if let photos = photos, let pages = pages {
                vc.picsData = photos
                vc.totalPages = pages
            } else {
                XCTFail()
            }
        }

        // then
        XCTAssertEqual(10, vc.collectionView.numberOfItems(inSection: 0))
        XCTAssertEqual(1, vc.totalPages)
    }

    func testShowsNoResults() {
        // given
        let mock = ApiMockNoResults.shared
        let vc = UIStoryboard.mainView()
        let _ = vc.view

        // when
        mock.search(query: "Rabbits", page: 1) { (photos, pages, err) in
            if let photos = photos, let pages = pages {
                vc.picsData = photos
                vc.totalPages = pages
            } else {
                XCTFail()
            }
        }

        // then
        XCTAssertEqual(0, vc.collectionView.numberOfItems(inSection: 0))
        XCTAssertEqual(0, vc.totalPages)
    }
}
